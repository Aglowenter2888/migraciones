<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDireccionIndividualizada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direccion_individualizada', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo_tesis');
            $table->string('grado');
            $table->date('fecha_inicio');
            $table->date('fecha_termino');
            $table->integer('numero_alumnos');
            $table->string('estado_direcion_individualizada');
            $table->string('curriculum_de_cuerpo_academico');
            $table->string('miembros');
            $table->string('lgacs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direccion_individualizada');
    }
}
