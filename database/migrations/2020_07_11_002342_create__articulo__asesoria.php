<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticuloAsesoria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_articulo__asesoria', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_del_estudio');
            $table->string('alcance_objetivo');
            $table->string('empresa');
            $table->string('pais');
            $table->string('estado_actual');
            $table->string('participantes');
            $table->string('curriculum_de_cuerpo_academico');
            $table->string('miembros');
            $table->string('lgacs');
            $table->date('fecha');
            $table->string('area_entrega');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_articulo__asesoria');
    }
}
