<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialDidactico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_material__didactico', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('autor');
            $table->string('titulo');
            $table->string('descripcion');
            $table->string('pais');
            $table->string('editorial');
            $table->string('proposito');
            $table->string('miembros');
            $table->string('lgacs');
            $table->date('fecha');
            $table->string('area_entrega');
            $table->string('curriculum_de_cuerpo_academico');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_material__didactico');
    }
}
