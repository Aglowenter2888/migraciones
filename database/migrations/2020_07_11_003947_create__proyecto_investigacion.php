<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectoInvestigacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_proyecto_investigacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo_proyecto');
            $table->string('nombre_patrocinador');
            $table->string('tipo_patrocinador');
            $table->string('curriculum_de_cuerpo_academico');
            $table->string('investigadores_partisipantes');
            $table->string('alumnos_partisipantes');
            $table->string('actividades_realizadas');
            $table->string('miembros');
            $table->string('lgacs');
            $table->date('fecha');
            $table->date('fecha_termino');
            $table->string('area_entrega');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_proyecto_investigacion');
    }
}
