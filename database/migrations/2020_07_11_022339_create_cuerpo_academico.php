<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuerpoAcademico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuerpo_academico', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_cuerpo_academico');
            $table->string('clave');
            $table->string('grado_consolidacion');
            $table->text('cultiva_cuerpo_academico');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuerpo_academico');
    }
}
