<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticuloArbitrado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulo_arbitrado', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('autor');
            $table->string('titulo');
            $table->string('estado_actual');
            $table->string('pais');
            $table->string('nombre_revista');
            $table->string('editorial');
            $table->string('valumen');
            $table->string('issn');
            $table->string('proposito');
            $table->string('miembros');
            $table->string('lgacs');
            $table->string('curriculum_de_cuerpo_academico');
            $table->date('año');
            $table->string('area_entrega');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulo_arbitrado');
    }
}
