<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstadiaEmpresas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estadia_empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_estadia');
            $table->string('programa_educativo');
            $table->integer('grado');
            $table->string('nombre_istitucion');
            $table->string('puntos_resolver');
            $table->string('resultados_obtenidos');
            $table->date('fecha_inicio');
            $table->date('fecha_termino');
            $table->integer('numero_alumnos');
            $table->integer('numero_horas');
            $table->string('estado_direcion_individualizada');
            $table->string('curriculum_de_cuerpo_academico');
            $table->string('miembros');
            $table->string('lgacs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estadia_empresas');
    }
}
