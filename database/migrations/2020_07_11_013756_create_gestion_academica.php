<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGestionAcademica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gestion_academica', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tipo_gestion');
            $table->string('nombre_estadia');
            $table->string('programa_educativo');
            $table->string('grado');
            $table->string('nombre_empresa_institucion');
            $table->string('puntos_criticos');
            $table->string('resultados_obtenidos');
            $table->date('fecha_inicio');
            $table->date('fecha_termino');
            $table->integer('numero_alumnos');
            $table->integer('numero_horas');
            $table->string('estado_direcion_individualizada');
            $table->string('curriculum_de_cuerpo_academico');
            $table->string('miembros');
            $table->string('lgacs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gestion_academica');
    }
}
