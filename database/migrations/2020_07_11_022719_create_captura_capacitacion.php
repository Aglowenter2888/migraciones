<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapturaCapacitacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('captura_capacitacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_articulo');
            $table->string('descripcion');
            $table->string('duracion');
            $table->date('fecha_dessarrollo');
            $table->string('area_entrega');
            $table->file('archivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('captura_capacitacion');
    }
}
