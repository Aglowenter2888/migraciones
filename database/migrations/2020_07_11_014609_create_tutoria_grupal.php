<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTutoriaGrupal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutoria_grupal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('numero_estudiantes');
            $table->string('nivel_educacion');
            $table->string('programa_educativo');
            $table->date('fecha_inicio');
            $table->date('fecha_termino');
            $table->string('tipo_titulaje');
            $table->string('estado_titulaje');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutoria_grupal');
    }
}
