<?php

namespace App\Http\Controllers;
use App\Document;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $file = Document::where('user_id', auth()->user()->id)->orderBy('id', 'DESC')->get();
        $count = Document::where('user_id', auth()->user()->id)->orderBy('id', 'DESC')->count();
        return view('home', compact('file'),compact('count') );
    }
}
