<?php

namespace App\Http\Controllers;

use App\Document;
use App\User;
use Illuminate\Http\Request;

class DocumentosController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function search(Request $request){
        $search = $request->get('search'); 
        $file=Document::where('titulo','like',"%$search%")
            ->orWhere('autor','like',"%$search%")
            ->orWhere('tipo','like',"%$search%")
            ->orWhere('cuatrimestre','like',"%$search%")
            ->orWhere('created_at','like',"%$search%")
           ->where('user_id', auth()->user()->id)->orderBy('id', 'DESC')->get();;

        return view('documentos.index',compact('file'));
    }
  
    public function fechas(Request $request){
        $fechas = $request->get('fechas');      
        $file=Document::where('cuatrimestre','like',"%$fechas%")->where('user_id', auth()->user()->id)->orderBy('id', 'DESC')->get();

        return view('documentos.index',compact('file'));
    }
    public function fechaActual(Request $request){
        $fechaActual = $request->get('fechaActual');      
        $file=Document::where('created_at','like',"%$fechaActual%")->where('user_id', auth()->user()->id)->orderBy('id', 'DESC')->get();

        return view('documentos.index',compact('file'));
    }
       public function index()
    {
        $file = Document::where('user_id', auth()->user()->id)->orderBy('id', 'DESC')->get();
        return view('documentos.index', compact('file'));
    }


    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(){
        return view('documentos.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(User $user, Request $request){
        $data = new Document;

        if($request->file('file'))
        {
            $file=$request->file('file');
            $filename=time().'.'.$file->getClientOriginalExtension();
            $request->file->move('storage/' , $filename);

            $data->file=$filename;
        }
        $data->user_id = auth()->id();
        $data->titulo=$request->titulo;
        $data->autor=$request->autor;
        $data->descripcion=$request->descripcion;
        $data->tipo=$request->tipo;
        $data->cuatrimestre=$request->cuatrimestre;
      //  $data->User()->associate($user)->save();
        $data->save();

        return redirect()->route('archivos.index');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id){
        $data=Document::find($id);

        return view('documentos.view',compact('data'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function download($file){
        return response()->download('storage/' .$file);
    }

    public function edit($id){
    //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id){
    //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id){
        $data = Document::find($id);
        $data->delete();

        return redirect()->route('archivos.index');
    }
}
