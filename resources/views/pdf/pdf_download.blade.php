
@section('Titulo', 'Repositorio de investigaciones ')
<body>
    <div id="content">
      <h2>Aqui va el titulo: <b> <span style="color:red">{{ ucfirst($Titulo) }}</span> </b></h2> 
      <h4>Aqui el Asunto: {{ $Asunto}} <br> Aqui va la fecha {{$Date}}</h4>
      <p>
         Esto es la descripcion:   {{ $Descripcion }}
      </p>
      <p style="text-align: center;">Este es el docente: {{ $Docente}}</p>
      <p style="text-align: center;">Esta es el Area: {{ $Area}}</p>
    </div>
  </body>