@extends('layouts.app')

@section('Titulo', 'Repositorio de investigaciones ')

@section('content')
    <div class="panel-header" style="background: #69bb85;">
        <div class="page-inner py-5">
            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                <div>
                    <h2 class="text-white pb-2 fw-bold">Dashboard</h2>
                    <h5 class="text-white op-7 mb-2">Sistema Repositorio de Producción Académica</h5>
                </div>
              
            </div>
        </div>
    </div>
    <div class="page-inner mt--5">
    
    <!-- Contenido del Dashboard -->
        <div class="row">
            <!-- Cuadros de menu -->
            <div class="col-sm-6 col-lg-3">
                <div class="card p-3 bg-info text-white ">
                    <div class="card-body p-3 text-center">
                        <div class="h1 m-0">
                            <!-- Icono -->
                            <i class="la flaticon-file"></i>
                        </div>
                        <div class="mb-3 text-white ">Documentos <br>realizados: 
                        <b>  {{$count}} </b>
                      
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card p-3 text-white" style="background: #69bb85;">
                    <div class="card-body p-3 text-center text-white ">
                        <div class="h1 m-0">
                            <!-- Icono -->
                            <i class="la flaticon-folder"></i>
                        </div>
                        <div class="text-white mb-3">CURP {{ auth()->user()->curp}}</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card p-3 bg-info">
                    <div class="card-body p-3 text-white  text-center">
                        <div class="h1 m-0">
                            <!-- Icono -->
                            <i class="la flaticon-users"></i>
                        </div>
                        <div class="text-white mb-3">Division<br> {{ auth()->user()->division}}</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card p-3" style="background: #69bb85;">
                    <div class="card-body p-3 text-center text-white ">
                        <div class="h1 m-0">
                            <!-- Icono -->
                            <i class="la flaticon-desk"></i>
                        </div>
                        <div class="text-white mb-3">Programa Educativo <br>{{ auth()->user()->grado}}</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Documentos Almacenados</div>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-right p-3">
                        <table  class="table table-striped table-responsive" >

<tr>
    <th>#</th>
    <th>Titulo</th>
    <th>Autor</th>
    <th>Tipo de Documento</th>
    <th>Periodo</th>
    <th>Fecha de Creacion</th>
    <th>Ver</th>
    </tr>
      @foreach ($file as $key=>$data)
      <tr>
          <td>{{++$key}}</td>
          <td>{{$data->titulo}}</td>
          <td>{{$data->autor}}</td>
          <td>{{$data->tipo}}</td>
          <td>{{$data->cuatrimestre}}</td>
          <td>{{$data->created_at}}</td>
          <td><a class="btn btn-success" href="/files/{{$data->id}}">ver</a></td>
    
          </tr>
      @endforeach
</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection


<!-- @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection -->
